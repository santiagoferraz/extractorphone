const express = require('express')
const puppeteer = require('puppeteer')
const olx = require('../olx/olx')

const router = express.Router()

router.get('/get-estados', async (req, res) => {
    const data = await olx.getEstados()
    res.send(data)
})

router.get('/get-cidades', async (req, res) => {
    const data = await olx.getCidades(req.query.url, req.query.uf)
    res.send(data)
})

router.get('/get-bairros', async (req, res) => {
    const data = await olx.getBairros(req.query.url, req.query.uf)
    res.send(data)
})

router.get('/get-categorias', async (req, res) => {
    const data = await olx.getCategorias()
    res.send(data)
})

router.get('/get-urls-anuncios', async (req, res) => {
    const data = await olx.getUrlsAnuncios(req.query.url)
    res.send(data)
})

router.post('/list-phones', async (req, res) => {
    const data = await olx.getPhones(req.body.urls)
    res.send(data)
})

module.exports = app => app.use('/olx-api', router)