const puppeteer = require('puppeteer');
const fs = require('fs');
const { throws } = require('assert');

async function getEstados() {
    const path = __dirname + '/json/estados.json';
    let estados;
    if (fs.existsSync(path)) {

        return fs.readFileSync(path, 'utf8')

    } else {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.setViewport({ width: 900, height: 600 });

        const url = "https://www.olx.com.br/";
        await page.goto(`${url}`);

        estados = await page.evaluate(() => {
            let estadosList = document.querySelectorAll('div[class="container"] a');
            let estados = [];
            estadosList.forEach((e) => {
                estados.push({ uf: e.text.toLowerCase(), url: e.getAttribute('href') })
            });
            return estados;
        });

        await browser.close();

        fs.writeFile(path, JSON.stringify(estados), err => {
            if (err) {
                console.error(err)
                return
            }
        });
        return estados;
    }
}

async function getCidades(url, uf) {
    url = url.replace(/"/g, '');
    uf = uf.replace(/"/g, '');

    const path = `${__dirname}/json/cidades-${uf}.json`;
    let cidades;
    if (fs.existsSync(path)) {

        return fs.readFileSync(path, 'utf8')

    } else {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.setViewport({ width: 900, height: 600 });

        await page.goto(`${url}`);

        cidades = await page.evaluate(() => {
            let cidadesList = document.querySelectorAll('a[data-lurker-detail="linkshelf_item"]');
            let cidades = [];
            cidadesList.forEach((e) => {
                cidades.push({ nome: e.text, url: e.getAttribute('href') })
            });
            return cidades;
        });

        await browser.close();

        fs.writeFile(path, JSON.stringify(cidades), err => {
            if (err) {
                console.error(err)
                return
            }
        });
        return cidades;
    }
}

async function getBairros(url, uf) {
    url = url.replace(/"/g, '');
    uf = uf.replace(/"/g, '');

    let b = url.split('/');
    b = b[b.length - 1];

    const path = `${__dirname}/json/bairros-${b}.json`;
    let bairros;
    if (fs.existsSync(path)) {

        return fs.readFileSync(path, 'utf8')

    } else {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.setViewport({ width: 900, height: 600 });

        await page.goto(`${url}`);

        bairros = await page.evaluate(() => {
            let bairrosList = document.querySelectorAll('a[data-lurker-detail="linkshelf_item"]');
            let bairros = [];
            bairrosList.forEach((e) => {
                bairros.push({ nome: e.text, url: e.getAttribute('href') })
            });
            return bairros;
        });

        await browser.close();

        fs.writeFile(path, JSON.stringify(bairros), err => {
            if (err) {
                console.error(err)
                return
            }
        });
        return bairros;
    }
}

async function getCategorias() {
    const path = __dirname + '/json/categorias.json';
    let categorias;
    if (fs.existsSync(path)) {

        return fs.readFileSync(path, 'utf8')

    } else {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.setViewport({ width: 900, height: 600 });

        const url = "https://www.olx.com.br/brasil";
        await page.goto(`${url}`);

        categorias = await page.evaluate(() => {
            let categoriasList = document.querySelectorAll('[class="sticky-inner-wrapper"] div div div div a');
            let categorias = [];
            categoriasList.forEach((e) => {
                let eArr = e.getAttribute('href').split('/');
                categorias.push({ nome: e.textContent, partUrl: eArr[eArr.length - 1] })
            });
            return categorias;
        });

        await browser.close();

        fs.writeFile(path, JSON.stringify(categorias), err => {
            if (err) {
                console.error(err)
                return
            }
        });
        return categorias;
    }
}

async function getUrlsAnuncios(url) {
    url = url.replace(/"/g, '');

    let urlsAnuncios;
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({ width: 900, height: 600 });

    await page.goto(`${url}`);

    urlsAnuncios = await page.evaluate(() => {
        let urlsAnunciosList = document.querySelectorAll('ul[id="ad-list"] li a[data-lurker-detail="list_id"]');
        let urlsAnuncios = [];
        urlsAnunciosList.forEach((e) => {
            urlsAnuncios.push({ url: e.getAttribute('href') })
        });
        return urlsAnuncios;
    });

    await browser.close();

    return urlsAnuncios;
}

async function getPhones(urls) {
    const launchOptions = {
        headless: true
        // , args: ["--start-maximized"]
        // , args: ["--user-data-dir=./Google/Chrome/User Data/"]
    };
    const browser = await puppeteer.launch(launchOptions);
    const page = await browser.newPage();
    await page.setViewport({ width: 1000, height: 600 });

    let dados = [];

    for (let i = 0; i < urls.length; i++) {
        await page.goto(urls[i]);

        // ESPERA O CARREGAMENTO
        await page.waitForSelector('[id="miniprofile"] div[color="#f9f9f9"][bordercolor="#d8d8d8"] span[color="dark"]');
        // CLICA EM MAIS INFORMAÇÕES (SE HOUVER)
        if (page.waitForSelector('div a[href="javascript:void(0)"]')) {
            await page.click('div a[href="javascript:void(0)"]');
        }
        // CLICA EM VER NUMERO DO ANÚNCIO (SE HOUVER)
        if (await page.$('p span[class][color="dark"][font-weight] a')) {
            await page.click('p span[class][color="dark"][font-weight] a')
        }
        // if (page.waitForSelector('p span[class][color="dark"][font-weight] a')) {
        //     await page.click('p span[class][color="dark"][font-weight] a')
        // }


        // // CLICA EM VER NUMERO DO PERFIL (SE HOUVER)
        // if (page.waitForSelector('[id="miniprofile"] a span[href]')) {
        //     await page.click('[id="miniprofile"] a span[href]');
        //     await tempoEspera(1000);
        // }
        

        dados.push(await page.evaluate(async () => {
            // Pegar descrição do anuncio
            let descricaoAnuncio = document.querySelector('p span[class][color="dark"][font-weight]').textContent

            // Filtrar números de celulares
            let rx = [];
            rx.push(/(\(?\d{2}\)?) ?[9]{1}\-?[4-9]{1}[0-9]{3}\-?[0-9]{4}/); // REGEX COM DDD ENTRE (dd)
            rx.push(/(\[?\d{2}\]?) ?[9]{1}\-?[4-9]{1}[0-9]{3}\-?[0-9]{4}/); // REGEX COM DDD ENTRE [dd]
            rx.push(/[9]{1}\-?[4-9]{1}[0-9]{3}\-?[0-9]{4}/); // REGEX SEM DDD COM 9 E -
            rx.push(/[9]{1} ?[4-9]{1}[0-9]{3}\-?[0-9]{4}/); // REGEX SEM DDD COM 9 E ESPAÇO

            let phoneNumber = [];

            // // BUSCAR NUMERO DO PERFIL
            // $elemTelPrincipal = document.querySelectorAll('[id="miniprofile"] a[href="#"][type="secondary"][font-weight] div')[1];
            // if ($elemTelPrincipal) {
            //     console.log($elemTelPrincipal.textContent)
            //     phoneNumber.push($elemTelPrincipal.textContent);
            // }

            rx.forEach(async (element) => {
                let phone = element.exec(descricaoAnuncio);
                if (phone && !phoneNumber.includes(phone[0].replace(/\D/gim, ''))) {
                    phoneNumber.push(phone[0].replace(/\D/gim, ''));
                }
            })

            // Reunir dados para retorno
            let dados;
            if (phoneNumber.length > 0) {
                // Set nome do anunciante
                let nomeAnunciante = document.querySelector('[id="miniprofile"] div[color="#f9f9f9"][bordercolor="#d8d8d8"] div div span[color="dark"][font-weight]').textContent;
                dados = [{ nome: nomeAnunciante }]

                // Set categorias do anuncio
                let categList = document.querySelectorAll('a[color="dark"][class][type="secondary"][font-weight]');

                let categStr = '';
                categList.forEach(async (e, k) => {
                    if (k === 0)
                        categStr += e.text
                    else
                        categStr += ' - ' + e.text
                });
                dados.push({ categoria: categStr.trim() })

                // Set telefones
                phoneNumber.forEach((e) => {
                    dados.push({ telefone: e });
                })
            }

            return dados;

        }));

    }

    await browser.close();

    dados = dados.filter(e => !!e);

    return dados;
}

async function tempoEspera(duration) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, duration)
    });
}

module.exports = {
    getEstados,
    getCidades,
    getBairros,
    getCategorias,
    getUrlsAnuncios,
    getPhones
};